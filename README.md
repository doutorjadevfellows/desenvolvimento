## Nossa empresa
A [Doutor Já](https://www.doutorja.com.br) é uma startup (Health Tech) que tem como missão facilitar o acesso à saúde de qualidade a população através  de soluções inovadoras.

Queremos conhecer programadore para integrar nosso time de devs e colaborar diariamente na implementação de novas soluções e melhorias de nossos projetos. 

Nós estamos muito focados atualmente em java script, node.js, mysql, angular ... mas somos agnósticos em relação a tecnologia e linguagens de programação.

## Como se candidatar
Envie o currículo para "querotrabalhar@doutorja.com.br" com assunto indicado na respectiva vaga.

## Local
Os nossos devs estão em Brooklin, São Paulo - SP.

